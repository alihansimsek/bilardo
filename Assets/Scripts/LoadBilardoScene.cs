﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadBilardoScene : MonoBehaviour {

    public void LoadByIndex(int sceneIndex) {
        SceneManager.LoadSceneAsync(sceneIndex);
    }
}
