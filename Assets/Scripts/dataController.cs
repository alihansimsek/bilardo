﻿using UnityEngine;
using System.IO;

public class dataController {


    private string gameDataProjectFilePath = "/StreamingAssets/data.json";

    void DataController() {
        Directory.CreateDirectory(Application.dataPath + gameDataProjectFilePath);
    }

    public GameData LoadGameData()
    {
        string filePath = Application.dataPath + gameDataProjectFilePath;

        if (File.Exists(filePath))
        {
            string dataAsJson = File.ReadAllText(filePath);
            GameData gameData = JsonUtility.FromJson<GameData>(dataAsJson);
            return gameData;
        }
        else
        {
            return null;
        }
    }

    public void SaveGameData(GameData gameData)
    {

        string dataAsJson = JsonUtility.ToJson(gameData);

        string filePath = Application.dataPath + gameDataProjectFilePath;

        Debug.Log("Saving as JSON: " + dataAsJson);

        File.WriteAllText(filePath, dataAsJson);

    }
}
