﻿using UnityEngine;

public class CameraBehavior : MonoBehaviour {

    public Transform ballCamTrans;
    public int ballCamXDistance, ballCamYDistance, ballCamZDistance;
    public Transform tableCamTrans;
    public int tableCamYDistance;
    public float mouseSensitivity = 100.0f;

    private int viewPos;
    private Vector3 cameraPos;
    private Vector3 tablePos;
    private Transform aimLine;

    private float rotY = 0.0f; // rotation around the y axis
    private float aimY = 0.0f;


    void Start() {
        
        tablePos = new Vector3(tableCamTrans.position.x, tableCamTrans.position.y + tableCamYDistance, tableCamTrans.position.z); //table position won't change in the game so it can be assigned at start
        aimLine = ballCamTrans.GetChild(0);
        ballView();
        
        
    }

    void Update() {

        if (viewPos == 1)       //if camera is in ballView
        {
            float mouseX = Input.GetAxis("Mouse X");

            rotY = mouseX * mouseSensitivity * Time.deltaTime;
            aimY += mouseX * mouseSensitivity * Time.deltaTime;

            transform.RotateAround(ballCamTrans.position, new Vector3(0.0f, 1.0f, 0.0f), 20 * Time.deltaTime * rotY);
        
            aimLine.RotateAround(ballCamTrans.position, new Vector3(0.0f, 1.0f, 0.0f), 20 * Time.deltaTime * rotY);
            
        }
    }

    public void ballView() //"Behind the ball" view while making the shot
    {
        cameraPos.Set(ballCamTrans.position.x + ballCamXDistance, ballCamTrans.position.y + ballCamYDistance, ballCamTrans.position.z + ballCamZDistance);
        transform.position = cameraPos;
        transform.LookAt(ballCamTrans);
        aimLine.rotation=(Quaternion.Euler(0, 0, 0));

        viewPos = 1;
    }

    public void tableView() //Top-down view of table when shot is made
    {
        transform.position = tablePos;
        transform.LookAt(tableCamTrans);
        viewPos = 2;
    }

}
