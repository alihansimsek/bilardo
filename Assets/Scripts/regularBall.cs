﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class regularBall : MonoBehaviour {

    private billiardSoundController soundController;
    void Start () {
        soundController = GameObject.Find("BilliardSoundController").GetComponent<billiardSoundController>();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "yellowBall" || collision.gameObject.tag == "redBall" || collision.gameObject.tag == "whiteBall")
        {
            soundController.ballClashed();
        }
    }
    }
