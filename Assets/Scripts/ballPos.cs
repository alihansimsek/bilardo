﻿
public class ballPos {

    public float x, z;

    public ballPos() {}

    public ballPos(float x, float z) {
        this.x = x;
        this.z = z;
    }

    public float X
    {
        get
        {
            return x;
        }

        set
        {
            x = value;
        }
    }

    public float Z
    {
        get
        {
            return z;
        }

        set
        {
            z = value;
        }
    }
}
