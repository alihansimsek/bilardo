﻿using UnityEngine;

public class whiteBallScript : MonoBehaviour {


    private gameController gameController;
    private billiardSoundController soundController;
    
    void Start () {
        gameController = GameObject.Find("GameController").GetComponent<gameController>();
        soundController = GameObject.Find("BilliardSoundController").GetComponent<billiardSoundController>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "yellowBall") {
            gameController.yHit();
            soundController.ballClashed();
        }
        if (collision.gameObject.tag == "redBall") {
            
            gameController.rHit();
            soundController.ballClashed();
        }
    }
}
