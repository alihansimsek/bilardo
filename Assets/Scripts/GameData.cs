﻿
[System.Serializable]
public class GameData{

    public int moves;
    public float timePassed;
    public int score;
    public bool isWon;

    public GameData()
    {
        moves = 0;
        score = 0;
        timePassed = 0f;
        IsWon = false;
    }

    public int Score
    {
        get
        {
            return score;
        }

        set
        {
            score = value;
        }
    }

    public int Moves
    {
        get
        {
            return moves;
        }

        set
        {
            moves = value;
        }
    }

    public float TimePassed
    {
        get
        {
            return timePassed;
        }

        set
        {
            timePassed = value;
        }
    }

    public bool IsWon
    {
        get
        {
            return isWon;
        }

        set
        {
            isWon = value;
        }
    }
}
