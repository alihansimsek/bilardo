﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameController : MonoBehaviour {

    public GameObject whiteBall,yellowBall,redBall;
    public GameObject powerBar;
    public int powerGainSpeed;
    public int winningScore;

    private int maxPower = 100;
    private int iteration = 0;
    private float timePassed = 0f;
    private float power = 0f;
    private bool replaying = false;
    private bool yellowHit, redhit;
    private bool isMoving, pointGained;
    private GameObject aimLine;
    private LineRenderer aimLineR;
    private Scrollbar scrollbar;
    private Rigidbody whiteRigid, yellowRigid, redRigid;
    private Transform aimLineT,whiteT,yellowT,redT;
    private Text scoreText, timeText, movesText, powerText;
    private GameData gameData;          //class that will be saved in JSON file
    private dataController dataCon;     //methods to save and load
    private CameraBehavior cameraBehavior;
    private List<ballPos> whitePos, yellowPos, redPos;      //position lists to show replays. rotations wont be cached to use less memory but can easily be implemented with more lists and modified ballPos 
    private Vector3 whiteTarget,yellowTarget,redTarget;     //target positions in replays
    private Button replayButton;



    void Start () {

        Cursor.visible = false;

        whitePos = new List<ballPos>();
        yellowPos = new List<ballPos>();
        redPos = new List<ballPos>();

        dataCon = new dataController();
        gameData = new GameData();

        whiteRigid = whiteBall.GetComponent<Rigidbody>();
        yellowRigid = yellowBall.GetComponent<Rigidbody>();
        redRigid = redBall.GetComponent<Rigidbody>();

        whiteT = whiteBall.GetComponent<Transform>();
        yellowT = yellowBall.GetComponent<Transform>();
        redT = redBall.GetComponent<Transform>();

        scrollbar = powerBar.GetComponent<Scrollbar>();
        powerText = powerBar.GetComponentInChildren<Text>();
        powerText.enabled = false;

        aimLine = GameObject.Find("Line");
        aimLineT = aimLine.GetComponent<Transform>();
        aimLineR = aimLine.GetComponent<LineRenderer>();
        
        cameraBehavior = GameObject.Find("Main Camera").GetComponent<CameraBehavior>();
        scoreText = GameObject.Find("Score").GetComponent<Text>();
        timeText = GameObject.Find("Time").GetComponent<Text>();
        movesText = GameObject.Find("Moves").GetComponent<Text>();
        replayButton = GameObject.Find("Button").GetComponent<Button>();

        replayButton.interactable = false;
        isMoving = false;
        pointGained = false;
    }

    void Update() {
        timePassed += Time.deltaTime;
        timeText.text = ("Time: " + (int)timePassed);

        if (Input.GetKey("space") && !isMoving)
            charge();
        if (Input.GetKeyUp("space") && !isMoving)
            shoot();

        if (replaying==false)
            record();
        else 
            replay();    
        
        if (whiteRigid.IsSleeping() && redRigid.IsSleeping() && yellowRigid.IsSleeping() && isMoving)   //end turn when movement stops
            moveEnd();

        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }

    private void charge()
    {
        replayButton.interactable = false;
        if (power < maxPower)
        {
            power = (power + Time.deltaTime * powerGainSpeed);
            scrollbar.size += (power * Time.deltaTime) / maxPower;       //power bar charging
            powerText.enabled = true;
            powerText.text = ("Power: " + ((int)power).ToString());
            Debug.Log("charging power: " + power);
        }
    }

    private void shoot()
    {
        clearRecord();              //Prepare for the next record when player has made another shot
        aimLineR.enabled = false;
        whiteRigid.AddForce(aimLineT.forward * power, ForceMode.Impulse);
        Debug.Log("shot made with " + (int)power + " power");
        power = 0;
        cameraBehavior.tableView();
        gameData.Moves++;
        isMoving = true;
        movesText.text = ("Moves: " + gameData.Moves);
    }

    private void FixedUpdate()
    {
        if (yellowHit && redhit && !pointGained)
            gainPoint();     
    }

    private void replay()
    {
        replayButton.interactable = false;
        aimLineR.enabled = false;
        whiteT.position = (new Vector3(whitePos[iteration].x, 0.5f, whitePos[iteration].z));
        redT.position = (new Vector3(redPos[iteration].x, 0.5f, redPos[iteration].z));
        yellowT.position = (new Vector3(yellowPos[iteration].x, 0.5f, yellowPos[iteration].z));
        if (whiteT.position == whiteTarget && yellowT.position == yellowTarget && redT.position == redTarget)
        {
            replaying = false;
            replayButton.interactable = true;
            iteration = 0;
            whiteRigid.detectCollisions = true;
            yellowRigid.detectCollisions = true;
            redRigid.detectCollisions = true;
            cameraBehavior.ballView();
            aimLineR.enabled = true;
        }
        else
            iteration++;
    }

    private void clearRecord()  
    {
        whitePos.Clear();
        yellowPos.Clear();
        redPos.Clear();
    }

    public void replayPrep()    //activated by replay button. pre replaying method.
    {
        cameraBehavior.tableView();
        
        whiteTarget = new Vector3(whiteT.position.x, whiteT.position.y,whiteT.position.z);  //caching current positions before replay so it knows when replay ends
        yellowTarget = new Vector3(yellowT.position.x, yellowT.position.y, yellowT.position.z);
        redTarget = new Vector3(redT.position.x, redT.position.y, redT.position.z);

        whiteRigid.detectCollisions = false;        //disabling collisions so score won't change when replaying a score
        yellowRigid.detectCollisions = false;
        redRigid.detectCollisions = false;

        replaying = true;   //bool for update()

        Debug.Log("REPLAYING");
    }

    private void record()
    {
        whitePos.Add(new ballPos { x=whiteT.position.x, z=whiteT.position.z });
        yellowPos.Add(new ballPos { x = yellowT.position.x, z = yellowT.position.z });
        redPos.Add(new ballPos { x = redT.position.x, z = redT.position.z });
    }

    private void gainPoint()
    {
        gameData.Score++;
        scoreText.text = ("Score: " + gameData.Score);
        pointGained = true;
        if (gameData.Score >= winningScore)
            gameWon();
    }

    private void moveEnd()
    {
        cameraBehavior.ballView();
        isMoving = false;
        yellowHit = false;
        redhit = false;
        pointGained = false;
        replayButton.interactable = true;
        scrollbar.size = 0;       //power bar reset
        powerText.enabled = false;
        aimLineR.enabled = true;
    }

    private void gameWon()
    {
        gameData.TimePassed = timePassed;
        gameData.isWon = true;
        dataCon.SaveGameData(gameData);
        SceneManager.LoadSceneAsync(0);
    }

    public void yHit() {        //yellow and red balls send collisions individually
        yellowHit = true;
        Debug.Log("yellow hit");
    }
    public void rHit()
    {
        redhit = true;
        Debug.Log("red hit");
    }
}
