﻿using UnityEngine;

public class billiardSoundController : MonoBehaviour {

    private AudioSource ballClash;
	void Start () {
        ballClash = GetComponent<AudioSource>();
	}

    public void ballClashed() {
        ballClash.Play();
    }
}
