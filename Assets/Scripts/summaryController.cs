﻿
using UnityEngine;
using UnityEngine.UI;

public class summaryController : MonoBehaviour {

    private GameData gameData;
    private dataController dataCon;
    private Text scoreText, timeText, movesText, resultText;

    // Use this for initialization
    void Start () {
        Cursor.visible = true;
        dataCon = new dataController();
        gameData = new GameData();
        scoreText = GameObject.Find("Score").GetComponent<Text>();
        timeText = GameObject.Find("Time").GetComponent<Text>();
        movesText = GameObject.Find("Moves").GetComponent<Text>();
        resultText = GameObject.Find("Result").GetComponent<Text>();

        loadSummary();

        scoreText.text = ("Score: " + gameData.Score);
        timeText.text = ("Time: " + (int)gameData.TimePassed);
        movesText.text = ("Moves: " + gameData.Moves);
        if (gameData.isWon)
            resultText.text = ("Game Won");
        else
            resultText.text = ("Game Lost");        //not achievable except the first time exe run

    }

    private void loadSummary()
    {
        gameData = dataCon.LoadGameData();
    }
}
